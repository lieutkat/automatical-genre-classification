# Automatical genre classification

### In short

This repository contains files with scripts and data which are related to reasearch in computational linguistics and has the title "Automatical genre classification".

The principle of script's work is based on keywords lists and text corpora approach. The text corpus consists of more than 11 million words in corpora and the total occuracy of genre determination is about 82%. 

To learn more about the program in all detail and the results of the reserach read this file till the end.


## Prerequisites

Python3 (no special libraries needed)

## The repository organization

The root folder contains: 
  - main_script.py
  - keyword lists for all genres
  - folder "scripts" (additional external python-scripts)
  - folder "results" (txt-files generated automatically which demonstrate how the algorithm worked in numbers)
  - several folders with pre-processed text data (cleaned, tokenized and prepared for working with the main script)

## The structure of main_script.py

### Get Keywords

```py
def get_keyword_list(): --> keywords_articles_ten: list, keywords_blogposts_ten: list, keywords_lawdocs_ten: list, keywords_news_ten: list
```
Reads `.txt` files with lists of keywords and get top-10 from them.

### Count Occurences

```py
def count_occurrences(n): --> research_article_flag: int, lawdoc_flag: int, blogpost_flag: int, news_article_flag: int
```
Counts the number of occurences of the words from keyword lists in each corpus. Returns numbers as flags (there are special coefficients which have different weight)

### Identify Genre

```py
def identify_genre(): --> result: str, estimation: str
```
Compares the gotten flags and estimates the probability that the text belongs to some specific genre.

### Export Results

```py
def export_results(): --> file
```
Does export of estimation into a txt-file.

## Usage

### Prepare Texts

Every single file that will be processed with the script should be a one `.txt` file containing tokenized words without litter. 

Prework on the corpus and texts inludes passing each to the following procedures:
`tokenization --> keywords extracting --> script running`

Do not forget to change all necessarily values and paths to let the algorithm work correctly.


  
