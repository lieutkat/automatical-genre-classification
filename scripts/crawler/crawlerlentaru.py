"""
a program-crawler which collects news-corpus (https://lenta.ru) for genre determination

! CHANGE THE VALUES: page, pattern and path to file to write

"""

import time
from urllib.request import Request, urlopen
from lxml.html import parse  # external lib
import chardet  # external lib

from bs4 import BeautifulSoup
from urllib.request import urlopen
import re


def find_links():

    # open url
    page = urlopen("https://lenta.ru/news/2019/02/12/")  # ! CHANGE path (YEAR, MONTH, DAY)
                                                         #  it should be a page with articles for one day
    soup = BeautifulSoup(page, "lxml")

    # get a part of html with links and other tags
    links_raw = soup.findAll(attrs={"class": "b-longgrid-column"})  # class span4 (include links for other days news)
                                                                    # class b-longgrid-column (certain day only)
    print(links_raw)
    print(type(links_raw))

    # convert from bs4.element.ResultSet to list and then to string
    links_converted = []
    print()
    for x in links_raw:
        print(x)
        links_converted.append(str(x))

    links_converted = str(links_converted)
    print()

    # search for link-pattern (for a certain day only!)
    # ! CHANGE DATE
    pattern = r'(news\/2019\/02\/12\/\w+)'
    links_taken = re.findall(pattern, links_converted)

    # number_of_links_taken = len(links_taken)
    # print(number_of_links_taken)
    for link in links_taken:  # list!
        print(link)

    return links_taken


def get_article():

    for i in range(0, (len(links_taken))):
        try:
            req = Request('https://lenta.ru/' + str(links_taken[i]), headers={'User-Agent': 'Mozilla/5.0'})

            page = urlopen(req)
            p = parse(page)
            root = p.getroot()

            # check the encoding with the help of external library (to use str.encode().decode() correctly)
            for_encoding_check = urlopen(req).read()
            print(chardet.detect(for_encoding_check))

            headline = root.xpath('//h1[@class="b-topic__title"][@itemprop="headline"]')[0]
            datetime = root.xpath('//time[@class="g-date"][@pubdate]')[0]
            content = root.xpath('//div[@class="b-text clearfix js-topic__text"][@itemprop="articleBody"]')[0]

            new_headline = headline.text_content()
            new_headline = new_headline.encode('ISO-8859-1').decode('utf-8', 'ignore')

            new_datetime = datetime.text_content()
            new_datetime = new_datetime.encode('ISO-8859-1').decode('utf-8')

            new_content = content.text_content()
            new_content = new_content.encode('ISO-8859-1').decode('utf-8')

            # deleting litter at the end
            litter = '''window._settings.components.eagleplayer = window._settings.components.eagleplayer || {};
            window._settings.components.eagleplayer.enabled = true;
            window._settings.components.eagleplayer.templates = {"multiplayer":["9116","10737","10738","10739","10740"],"scroll":["10409","10410","10737","10738","10814"]};
            window._settings.components.eagleplayer.relatedVideosHeight = 185;Что происходит в России и в мире? Объясняем на нашем YouTube-канале. Подпишись!'''
            litter_2 = '''Что происходит в России и в мире? Объясняем на нашем YouTube-канале. Подпишись!'''
            litter_3 = '''Еще больше интересного в нашем  Instagram. Подпишись!'''
            litter_4 = r'(#форма обратной связи)(\n*+.+\n*)+'
            litter_5 =  '''window._settings.components.eagleplayer = window._settings.components.eagleplayer || {};
window._settings.components.eagleplayer.enabled = true;
window._settings.components.eagleplayer.templates = {"multiplayer":["9116","10737","10738","10739","10740"],"scroll":["10409","10410","10737","10738","10814"]};
window._settings.components.eagleplayer.relatedVideosHeight = 185;'''
            litter_6 = '''
  
  
  
  
    :focus {
      outline: none;
    }
    .user-news {
      position: relative;
    }
    .user-news__wrap {
      display: -webkit-flex;
      display: -moz-flex;
      display: -ms-flex;
      display: -o-flex;
      display: flex;
      -webkit-flex-direction: column;
      -moz-flex-direction: column;
      -ms-flex-direction: column;
      -o-flex-direction: column;
      flex-direction: column;
      color: #FFF;
      -ms-align-items: center;
      align-items: center;
      background-color: #292929;
      border-radius: 3px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      padding: 15px 10px 20px;
      position: relative;
      z-index: 1;
    }
    .user-news::before, .user-news::after{
      display: block;
      content: '';
      position: absolute;
      border-radius: 3px;
    }
    .user-news::before {
      width: calc(100% - 36px);
      height: 14px;
      bottom: -11px;
      left: 18px;
      background-color: #F3F3F3;
    }
    .user-news::after {
      width: calc(100% - 16px);
      height: 8px;
      bottom: -5px;
      left: 8px;
      background-color: #D6D6D6;
    }
    .user-news__icon {
      width: 40px;
      height: 40px;
      margin-bottom: 5px;
    }
    .user-news__text {
      text-align: center;
    }
    .user-news__title {
      font: bold 18px/24px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      margin: 0 10px 10px;
    }
    .user-news__description {
      font: 14px/18px 'PT Serif', Georgia, Arial, serif;
      opacity: 0.5;
      margin-bottom: 15px;
    }
    .user-news__btn {
      font: bold 12px/26px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      letter-spacing: 1.2px;
      text-transform: uppercase;
      padding: 5px 16px;
      background-color: #CC3333;
      transition: background-color .2s ease;
      border-radius: 3px;
      cursor: pointer;
    }
    .user-news__btn:hover {
      background-color: #B72E2E;
    }
    /* конец стилей плашки сниппета */
    .user-news__overlay, .user-news__form {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      z-index: 1000;
    }
    .user-news__overlay {
      background-color: #292929;
      opacity: 0.6;
      cursor: pointer;
    }
    .user-news__overlay._is-hidden {
      visibility: hidden;
    }
    .user-news__form {
      display: -webkit-flex;
      display: -moz-flex;
      display: -ms-flex;
      display: -o-flex;
      display: flex;
      -webkit-flex-direction: column;
      -moz-flex-direction: column;
      -ms-flex-direction: column;
      -o-flex-direction: column;
      flex-direction: column;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      background-color: #F3F3F3;
      top: -70px;
      transform: translateY(70px);
      padding: 5px 10px 80px;
      margin: 0;
      overflow: hidden;
      -webkit-overflow-scrolling: touch;
      font-size: 0;
    }
    .user-news__form._is-hidden {
      visibility: hidden;
    }
    .user-news__form-title {
      text-align: center;
      font: 20px/28px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      margin: 15px 0 10px;
    }
    .user-news__form-title._success {
      padding: 75px 60px;
      margin: auto;
    }
    .user-news__form-story-wrap {
      position: relative;
      margin-bottom: 30px;
    }
    .user-news__form-story {
      display: block;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      width: 100%;
      min-height: 162px;
      max-height: 70vh;
      resize: none;
      border: none;
      background-color: #FFF;
      border-radius: 3px;
      padding: 5px 5px 30px;
      font: 14px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      margin: 0;
    }
    .user-news__form-story-counter {
      position: absolute;
      right: 10px;
      bottom: 5px;
      font: 12px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      color: #292929;
      opacity: 0.2;
    }
    .user-news__form-story-counter._shifted {
      right: 20px;
    }
    .user-news__form-story-counter::before {
      content: 'Осталось символов: '
    }
    .user-news__form-story-counter._overflow {
      opacity: 1;
      color: #C33;
    }
    .user-news__form-contacts {
      margin-bottom: 20px;
    }
    .user-news__form-group {
      position: relative;
      font: 16px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      padding-bottom: 16px;
      margin-bottom: 5px;
    }
    .user-news__form-label {
      opacity: 0.2;
      position: absolute;
      top: 5px;
      left: 1px;
      cursor: text;
      transition: top .2s ease, font-size .2s ease;
    }
    .user-news__form-input {
      font: 16px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      border: none;
      border-bottom: 1px solid #D5D5D5;
      border-radius: 0;
      padding: 1px 1px 3px 1px;
      background-color: transparent;
      outline: none;
      width: 100%;
      margin: 0;
    }
    .user-news__form-input:focus + .user-news__form-label,
    .user-news__form-input._filled + .user-news__form-label {
      top: 24px;
      font-size: 12px;
    }
    .user-news__form-error {
      position: absolute;
      width: 100%;
      background-color: #F3F3F3;
      font: 16px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      color: #CC3333;
      top: 27px;
      left: 0;
      right: 0;
      font-size: 12px;
    }
    .user-news__form-error._story-error {
      top: auto;
      bottom: 0;
    }
    .user-news__form-error._submit-error {
      top: -20px;
      bottom: auto;
    }
    .user-news__form-error._is-hidden {
      visibility: hidden;
    }
    .user-news__form-submit-wrap {
      position: relative;
    }
    .user-news__form-submit {
      -moz-appearance: none;
      -webkit-appearance: none;
      font: bold 12px/26px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      letter-spacing: 1px;
      color: #FFFFFF;
      text-transform: uppercase;
      background-color: #CC3333;
      padding: 6px 10px;
      margin: 0;
      border: none;
      outline: none;
      border-radius: 3px;
      cursor: pointer;
      width: 114px;
      transition: background-color .2s ease;
    }
    .user-news__form-submit:hover {
      background-color: #B72E2E;
    }
    .user-news__form-submit._with-margin {
      margin-bottom: 30px;
    }
    .user-news__form._success {
      margin: auto;
    }
    .user-news__form-close {
      position: absolute;
      top: 5px;
      right: 5px;
      cursor: pointer;
      transition: opacity 0.2s ease;
    }
    .user-news__form-close:hover {
      opacity: 0.8;
    }
    /* desktop snippet styles: */
    @media (min-width: 600px) {
      .user-news::before, .user-news::after {
        display: none;
      }
      .user-news__wrap {
        -webkit-flex-direction: row;
        -moz-flex-direction: row;
        -ms-flex-direction: row;
        -o-flex-direction: row;
        flex-direction: row;
        justify-content: space-between;
        padding: 15px 20px 20px 15px;
      }
      .user-news__icon {
        margin: 0;
      }
      .user-news__title {
        margin: 0 0 5px 0;
        line-height: 28px;
      }
      .user-news__text {
        width: 384px;
        text-align: left;
      }
      .user-news__description {
        margin-bottom: 0;
      }
      .user-news__btn {
        -ms-align-self: flex-end;
        align-self: flex-end;
      }
      .user-news__btn_extend {
        display: none;
      }
    }
    /* desktop form styles */
    .user-news__form._desktop {
      width: 880px;
      height: 80vh;
      margin: auto;
      padding: 30px 40px;
    }
    .user-news__form._desktop._success {
      height: 220px;
    }
    .user-news__form._desktop .user-news__form-title {
      text-align: left;
      margin: 0 0 10px;
    }
    .user-news__form._desktop .user-news__form-title._success {
      padding: 70px;
      margin: auto;
      text-align: center;
    }
    .user-news__form._desktop .user-news__form-story {
      min-height: calc(80vh - 232px);
      padding: 8px 8px 30px;
      font: 14px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .user-news__form._desktop .user-news__form-story-counter {
      bottom: 10px;
    }
    .user-news__form._desktop .user-news__form-contacts {
      display: -webkit-flex;
      display: -moz-flex;
      display: -ms-flex;
      display: -o-flex;
      display: flex;
    }
    .user-news__form._desktop .user-news__form-group {
      margin-right: 20px;
      width: 100%;
    }
    .user-news__form._desktop .user-news__form-group:last-child {
      margin-right: 0;
    }
    .user-news__form._desktop .user-news__form-input {
      padding: 1px 1px 5px 1px;
    }
    .user-news__form._desktop .user-news__form-input:focus + .user-news__form-label,
    .user-news__form._desktop .user-news__form-input._filled + .user-news__form-label {
      top: 24px;
      font-size: 12px;
    }
    .user-news__form._desktop .user-news__form-label {
      top: 1px;
    }
    .user-news__form._desktop .user-news__form-error._story-error {
      bottom: -20px;
    }
    .user-news__form._desktop .user-news__form-submit {
      margin: 0;
    }
    .user-news__form._desktop .user-news__form-close {
      top: 10px;
      right: 10px;
    }
  


  
    
      
        
          
            
            
            
            
            
            
            
          
        
      
      
        
          Поделитесь своей историей
        
        
          Редакция ищет очевидцев и участников Афганской войны. Весной в прокат выходит фильм «Братство» режиссера Павла Лунгина — первый правдивый взгляд в кино на события тех лет. Перед выходом картины мы предлагаем вам рассказать свою историю о той войне.
        
      
      
        Прислать
        историю
      
    
  
  
  
    
      Моя история
    
    
      
      20000
      
    
    
      
        
        Меня зовут
        
      
      
        
        Мой телефон или e-mail
        
      
      
        
        Мой город
      
    
    
      
      Не удалось отправить. Попробуйте повторить позже
    
    
      
      
    
  
  
    (function() {
      var showFormBtn = document.querySelector('.js-user-news-btn');
      var overlay = document.querySelector('.js-user-news-overlay');
      var form = document.querySelector('.js-user-news-form');
      var closeFormBtn = form.querySelector('.js-user-news-close');
      var story = form.querySelector('.js-user-news-story');
      var emailOrPhone = document.getElementById('user-news-communication');
      var counter = form.querySelector('.js-user-news-counter');
      var textInputs = form.querySelectorAll('.js-user-news-input');
      var sendFormBtn = form.querySelector('.js-user-news-submit');
      var screenHeightWithBars = window.innerHeight;
      var isDesktop = window.location.hostname.indexOf('m.lenta') < 0;

      function resizeStoryBox() {
        !isDesktop && window.innerHeight !== screenHeightWithBars ? sendFormBtn.classList.add('_with-margin') : sendFormBtn.classList.remove('_with-margin');
        var formElementsHeight = isDesktop ? 232 : (332 + sendFormBtn.parentNode.offsetHeight);
        // высота элементов формы (всех кроме textarea) и их отступов для десктопа и мобилки
        story.style.height = (form.offsetHeight - formElementsHeight) + 'px';
      }

      function showForm() {
        overlay.classList.remove('_is-hidden');
        form.classList.remove('_is-hidden');
        story.focus();
        resizeStoryBox();
      }

      function closeForm() {
        overlay.classList.add('_is-hidden');
        form.classList.add('_is-hidden');
      }

      function updateStoryCounter() {
        counter.textContent = story.maxLength - story.textLength;
        story.maxLength <= story.textLength ? counter.classList.add('_overflow') : counter.classList.remove('_overflow');
        story.scrollTop = story.scrollHeight;
        story.scrollHeight > story.offsetHeight ? counter.classList.add('_shifted') : counter.classList.remove('_shifted');
      }

      function keepInputFilled(input) {
        var inputError = input.parentNode.querySelector('.js-user-news-error');
        input.addEventListener('input', function() {
          input.value.length === 0 ? input.classList.remove('_filled') : input.classList.add('_filled');
          inputError && inputError.classList.add('_is-hidden');
        });
      }

      function validateEmailOrPhone(input) {
        var emailRegexp = /[а-яА-ЯЁё\w\.\-\+:]+@[а-яА-ЯЁё\w\.\-\+:]+/;
        var inputError = input.parentNode.querySelector('.js-user-news-error');
        if (emailRegexp.test(input.value) || input.value.split('').filter(Number).join('').length >= 10) {
          return true;
        } else if (input.value === '') {
          inputError.textContent = 'Обязательное поле для ввода';
        } else {
          inputError.textContent = 'Номер/e-mail введен не правильно';
        }
        inputError.classList.remove('_is-hidden');
        return false;
      }

      function validateNotEmpty(input) {
        var inputError = input.parentNode.querySelector('.js-user-news-error');
        if (input.value.length !== 0) {
          inputError.classList.add('_is-hidden');
          return true;
        }
        inputError.textContent = 'Обязательное поле для ввода';
        inputError.classList.remove('_is-hidden');
        return false;
      }

      function validateForm() {
        var formIsValid = true;
        var fieldsToValidate = Array.prototype.slice.call(document.querySelectorAll('.js-user-news-validate'), '');
        formIsValid = fieldsToValidate.map(validateNotEmpty).reduce(function(a, b) {
          return a && b;
        });
        return formIsValid && validateEmailOrPhone(emailOrPhone);
      }

      function sendForm(e) {
        e.preventDefault();
        var formIsValid = validateForm();
        if (formIsValid) {
          var request = new XMLHttpRequest();
          var requestUrl = '/user-story';
          var requestData = {
            user_name: document.getElementById('user-news-name').value,
            user_email_or_phone: emailOrPhone.value,
            user_city: document.getElementById('user-news-city').value,
            user_story: story.value,
            url: window.location.href
          }
          var encodedRequestData = Object.keys(requestData).map(function(key) {
            return key + '=' + encodeURIComponent(requestData[key]);
          }).join('&');

          request.open('POST', requestUrl, true);
          request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
          request.onreadystatechange = function() {
            if (request.readyState === 4 && request.status === 200) {
              var successBlock = document.createElement('div');
              successBlock.classList = 'user-news__form _success';
              successBlock.innerHTML = '<div class="user-news__form-title _success">История успешно отправлена</div>';
              successBlock.appendChild(closeFormBtn);
              form.innerHTML = '';
              form.classList.add('_success');
              form.appendChild(successBlock);
            } else {
              document.getElementById('user-news-submit-error').classList.remove('_is-hidden');
            }
          };
          request.send(encodedRequestData);
          return true;
        }
        return false;
      }

      isDesktop && form.classList.add('_desktop');

      showFormBtn.addEventListener('click', showForm);
      closeFormBtn.addEventListener('click', closeForm);
      overlay.addEventListener('click', closeForm);
      story.addEventListener('input', updateStoryCounter);
      sendFormBtn.addEventListener('click', sendForm);
      Array.prototype.slice.call(textInputs, '').forEach(keepInputFilled);
    })();
  

'''

            new_content = new_content.replace(litter, '')
            new_content = new_content.replace(litter_2, '')
            new_content = new_content.replace(litter_3, '')
            new_content = new_content.replace(litter_4, '')
            new_content = new_content.replace(litter_5, '')
            new_content = new_content.replace(litter_6, '')

            print(new_headline, '\n', new_datetime, '\n', new_content)

            f = open('articles/12febr_' + str(i) + '.txt', 'w+', encoding='utf-8')
            # CAREFUL! Everytime you change the date also CHANGE the path (or you'll lose your files)

            f.write(new_datetime + '\n')
            f.write(new_headline + '\n')
            f.write(new_content + '\n')
            f.close()

            print('done')
            time.sleep(1)
        except Exception as e:
            print(e)
            print('fail')


links_taken = find_links()
# print('Result', links_taken)
get_article()
