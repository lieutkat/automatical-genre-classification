 10:59, 25 февраля 2019
Появилось видео сместа убийства стрелявшего пополицейским россиянина
Следак шепнул / TelegramОперативное видео с места происшествия, где житель Астраханской области обстрелял полицейских и был ими убит, опубликовал Telegram-канал «Следак шепнул».На кадрах видно ружье, с которым 42-летний мужчина напал на сотрудников правоохранительных органов, и его тело.При обыске в доме погибшего в селе Бахтемир была обнаружена лаборатория по выращиванию наркосодержащих растений. Изъятое направлено на экспертизу.Инцидент произошел 24 февраля. Местные жители вызвали полицию в связи с тем, что по селу разгуливает неизвестный с ружьем. Прибывшие на место сотрудники правоохранительных органов обнаружили его около остановки. Они стали уговаривать мужчину сдать оружие, но тот открыл по полицейским стрельбу. Один из сотрудников правоохранительных органов был ранен. Его коллеги ликвидировали нападавшего ответным огнем.Обратная связь с отделом «Силовые структуры»:


Если вы стали свидетелем важного события, у вас есть новость или идея для материала, напишите на этот адрес: crime@lenta-co.ru
 
