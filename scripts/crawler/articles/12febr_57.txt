 12:35, 12 февраля 2019
Массовая драка между чеченцами итатарами вкафе попала навидео
Telegram / URA.ruВидео массовой драки в кафе «Вояж» в Тобольске Тюменской области опубликовало издание Ura.ru в своем Telegram-канале.Побоище между представителями татарской и чеченской диаспор произошло ночью 9 февраля. Оно началось на танцплощадке, затем драчуны стали крушить мебель.Конфликт начался с бытовой ссоры двух посетителей кафе, а затем им на подмогу пришли друзья.Полиция установила всех участников драки, их доставили в местный отдел МВД для разбирательства. Возбуждены уголовные дела по статьям 111 («Умышленное причинение тяжкого вреда здоровью») и 116 УК РФ («Побои»).Обратная связь с отделом «Силовые структуры»:


Если вы стали свидетелем важного события, у вас есть новость или идея для материала, напишите на этот адрес: crime@lenta-co.ru
Больше мрачных и странных новостей в Telegram-канале «Лента дна». Подписывайтесь!


