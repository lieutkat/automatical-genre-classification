 11:05, 19 февраля 2019
Избитого российскими школьниками прокурора заподозрили волжи
Жители Ханты-Мансийского автономного округа (ХМАО) решили обратиться к генпрокурору Юрию Чайке с просьбой проверить на детекторе лжи подчиненного, утверждающего, что его избили школьники. Об этом сообщил Ura.ru осведомленный источник.«Из-за слов прокурора, которые ничем не подтверждены, за решеткой уже третий месяц сидит 11-классник, который ни в чем не виноват. Сам школьник готов тоже пройти полиграф», — отметил собеседник уральского агентства.Мать обвиняемого Елена Самоловова подтвердила, что такое письмо написано и скоро будет направлено в Москву.Материалы по теме08:08 — 19 июня 2015Под колесами правосудияО том, как владимирские судьи не дали ход делу о ДТП со своим коллегойСогласно материалам дела, прокурор Игорь Банников был избит группой подростков во дворе своего дома. Инцидент произошел в конце 2018 года. По другой версии, сотрудник надзорного ведомства был пьян и сам стал конфликтовать с молодыми людьми.Фигурантами уголовного дела о нападении на представителя закона стали двое юношей. Один из них — ученик одиннадцатого класса. Через несколько дней его отца задержали, предположительно, за попытку дать взятку должностному лицу.В региональной прокуратуре ХМАО, по данным агентства, дело официально не комментируют.Ранее в январе сообщалось, что краснодарский судья Арсен Крикоров, сбивший девушку на пешеходном переходе и запугивавший очевидцев произошедшего, подал в отставку.*** 


Обратная связь с отделом «Общество»: 


Если вы стали свидетелем важного события, у вас есть новость, вопросы или идея для материала, напишите на этот адрес: russia@lenta-co.ru
