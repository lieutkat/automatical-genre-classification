 15:41, 13 февраля 2019
Тысячи мертвых каракатиц вынесло напопулярный пляж
bangkokpost.comВ Чили на популярный у туристов пляж вынесло тысячи мертвых каракатиц. На видео обратил внимание информационный портал UNTV News.Ролик был снят в воскресенье, 10 февраля, на пляже Баия Инглеса, расположенном неподалеку от города Кальдера.Местные жители уверяют, что прежде не видели ничего подобного. Экологи пытаются выяснить причины аномалии: они предполагают, что моллюски погибли из-за загрязнения воды. Граждан попросили не есть мертвых каракатиц, так как это может быть опасно для здоровья.Власти озабочены тем, что случившееся может негативно повлиять на рыболовную отрасль — один из основных источников заработка региона.В июне сообщалось о сотнях мертвых акулят, вынесенных на гавайский пляж. Предположительно они запутались в сетях у прибрежного ресторана, и от них решили избавиться.Больше мрачных и странных новостей в Telegram-канале «Лента дна». Подписывайтесь!


