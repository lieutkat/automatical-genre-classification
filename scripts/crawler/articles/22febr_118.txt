 18:06, 22 февраля 2019
Угрожавший россиянке лопатой депутат лишился должности
Зампредседатель гордумы Дзержинска Игорь Крашенинников лишился своей должности после публикации видео, на котором он угрожает женщине лопатой и оскорбляет ее из-за мелкого ДТП. Об этом сообщается на сайте нижегородского отделения партии «Единая Россия».Также Крашенинников исключен из местного политсовета «за хамское поведение». В «Единой России» решили, что он, будучи членом партии, своими действиями нанес партийной структуре репутационный ущерб. При этом мужчина останется депутатом.«Наказание суровое, человек лишен всех руководящих должностей. Свою вину он в полной мере осознает», — отметил секретарь регионального отделения партии Денис Москвин.В сообщении сказано, что Крашенинников назвал решение сопартийцев справедливым и заслуженным.Видео дорожного конфликта депутата с жительницей Дзержинска появилось в сети несколькими днями ранее. На кадрах женщина обвиняла Крашенинникова в том, что он задел ее машину. В ответ он достал из багажника лопату для уборки снега и направил ее на оппонентку. Оба они начали оскорблять друг друга. В итоге мужчина отказался осматривать чужой автомобиль на предмет повреждений, не стал ждать ГИБДД и уехал.*** 


Обратная связь с отделом «Общество»: 


Если вы стали свидетелем важного события, у вас есть новость, вопросы или идея для материала, напишите на этот адрес: russia@lenta-co.ru
