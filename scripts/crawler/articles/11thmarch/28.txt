 10:45, 11 марта 2019
Снегопад помешал футболисту забить впустые ворота
window._settings.components.eagleplayer = window._settings.components.eagleplayer || {};
window._settings.components.eagleplayer.enabled = true;
window._settings.components.eagleplayer.templates = {"multiplayer":["9116","10737","10738","10739","10740"],"scroll":["10409","10410","10737","10738","10814"]};
window._settings.components.eagleplayer.relatedVideosHeight = 185;Guardian Football / YouTubeНападающий «Ганновера» Генки Харагути не забил мяч в пустые ворота «Байера» в матче чемпионата Германии из-за снегопада. Видео появилось на YouTube-канале Guardian Football.При счете 0:2 Харагути вышел один на один, обыграл голкипера соперника и направил мяч в пустые ворота. Снаряд остановился на линии ворот из-за выпавшего во время матча снега.Встреча завершилась поражением «Ганновера» со счетом 2:3. Харагути не забил ни одного мяча в нынешнем сезоне чемпионата Германии.
