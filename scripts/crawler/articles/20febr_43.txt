 12:03, 20 февраля 2019
Россияне устроили самосуд над терроризировавшим школу пятиклассником
В приморском городе Большой Камень после безуспешных жалоб в полицию и руководству школы № 2 родители пятиклассников устроили самосуд над одним из учеников, сообщает DEITA.RU.«На данный момент полиция ведет расследование, о подробностях происшествия я говорить не могу», — заявила изданию директор школы.По словам источника, мальчика окунули головой в унитаз за то, что он избил девочку, которая теперь находится в больнице с подозрением на разрыв селезенки.Всех участников инцидента доставили в полицию.Накануне конфликта с ученицей, как пишет издание, родители писали директору коллективное заявление с требованием принять срочные меры к учащемуся, поведение которого «вызывает опасения за жизнь и здоровье наших детей». В заявлении, в частности, говорилось, что мальчик регулярно вымогает деньги и портит имущество других учеников, а также оскорбляет, унижает и бьет их.Никакой реакции на обращение не последовало.В декабре прошлого года мать одного из школьников рассказывала журналистам, что этот пятиклассник приходит на занятия с ножом, режет куртки одноклассникам и угрожает им самим. Как оказалось, ребенок живет с бабушкой, так как его родители лишены родительских прав.После обращения к СМИ директриса предложила женщине перевести ребенка в другую школу и назвала все обвинения сплетнями. В региональном департаменте образования тогда рассказали изданию, что держат ситуацию на контроле, но отрицают информацию о посещении школы учеником с ножом.*** 


Обратная связь с отделом «Общество»: 


Если вы стали свидетелем важного события, у вас есть новость, вопросы или идея для материала, напишите на этот адрес: russia@lenta-co.ru
