 19:23, 12 марта 2019
Авиакомпании массово отказались отмодели рухнувшего вАфрике Boeing
Минимум 27 авиакомпаний отказались от эксплуатации Boeing 737 Max 8 после крушения самолета Ethiopian Airlines в Африке 10 марта. Об этом сообщает New York Times.Среди них оказались China Southern Airlines, Norwegian Air, Iceland Air, Air China, Turkish Airlines, Shanghai Airlines, Lion Air, Oman Air, Ethiopian Airlines, Lucky Air, Royal Air Maroc, Garuda Indonesia и другие.Кроме того, свое воздушное пространство для Boeing 737 Max 8 закрыли некоторые страны, в том числе Германия, Великобритания, Австралия, Сингапур. Малайзия и Оман. Позже к ним присоединились Франция, Ирландия и Австрия.Самолет Boeing 737 Max 8 авиакомпании Ethiopian Airlines разбился утром 10 марта через шесть минут после вылета из аэропорта Аддис-Абебы. Все 157 человек, находившихся на борту, погибли. Среди жертв авиакатастрофы — граждане 33 стран, в том числе трое россиян — Екатерина Полякова, Александр Поляков и Сергей Вяликов, сообщило посольство России в Эфиопии в Twitter.window._settings.components.eagleplayer = window._settings.components.eagleplayer || {};
window._settings.components.eagleplayer.enabled = true;
window._settings.components.eagleplayer.templates = {"multiplayer":["9116","10737","10738","10739","10740"],"scroll":["10409","10410","10737","10738","10814"]};
window._settings.components.eagleplayer.relatedVideosHeight = 185;*** 


Обратная связь с отделом «Путешествия»: 


Если вы стали свидетелем важного события, у вас есть новость или идея для материала, напишите на этот адрес: travel@lenta-co.ru
