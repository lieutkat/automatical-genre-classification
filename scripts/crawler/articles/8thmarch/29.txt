 15:06,  8 марта 2019
Грузовик перевернулся изалил пивом автостраду
window._settings.components.eagleplayer = window._settings.components.eagleplayer || {};
window._settings.components.eagleplayer.enabled = true;
window._settings.components.eagleplayer.templates = {"multiplayer":["9116","10737","10738","10739","10740"],"scroll":["10409","10410","10737","10738","10814"]};
window._settings.components.eagleplayer.relatedVideosHeight = 185;Twitter / ABCНа автостраде к востоку от Лос-Анджелеса перевернулся грузовик с пивом. В результате дорога оказалась усыпана десятками упаковок марки Modelo Especial, пишет NBC Los Angeles.Инцидент произошел утром 7 марта. 25-летний водитель грузовика Peterbilt из города Инглвуд пытался резко затормозить. Он вырулил на съезд, но не справился с управлением и скатился по насыпи.Часть бутылок разбилась, из-за чего на дороге образовались пивные лужи. Сообщается также, что в результате инцидента произошло небольшое возгорание. Водитель большегруза остался жив.
