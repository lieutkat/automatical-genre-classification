 23:34,  8 марта 2019
Еще один министр покинул Трампа
Министр ВВС США Хезер Уилсон решила покинуть свой пост. Об этом она написала на своей странице в Twitter.Она уточнила, что уходит со своей должности, чтобы возглавить Университет штата Техас в Эль-Пасо.Президент США Дональд Трамп поблагодарил Уилсон за службу. Он отметил, что та проделала «совершенно фантастическую работу и также будет великолепна в крайне важном мире высшего образования».Министр ВВС — гражданская должность в Пентагоне. Кандидат выдвигается президентом и утверждаются Сенатом, однако подчиняется министру обороны США. В его зону ответственности входит ведение всех вопросов повседневной жизни военно-воздушных сил.window._settings.components.eagleplayer = window._settings.components.eagleplayer || {};
window._settings.components.eagleplayer.enabled = true;
window._settings.components.eagleplayer.templates = {"multiplayer":["9116","10737","10738","10739","10740"],"scroll":["10409","10410","10737","10738","10814"]};
window._settings.components.eagleplayer.relatedVideosHeight = 185;
