 11:28,  1 марта 2019
Кормившего солдат вСирии россиянина отказались признать ветераном
Россиянину Сергею Сотникову, который провел четыре командировки в Сирии в качестве повара, отказались присвоить статус ветерана боевых действий, пишет «Коммерсантъ» в пятницу, 1 марта.Мужчина кормил летчиков на авиабазе и был отмечен благодарностью президента России Владимира Путина за содействие в решении специальных задач, возложенных на Вооруженные силы.Материалы по теме00:04 —  1 февраля 2018Умру за родину. ДорогоРоссийские наемники погибают за границей, а дома им грозит тюрьмаСотников пытается отсудить свое право на ветеранское удостоверение и льготы. Его апелляцию принял к рассмотрению Оренбургский областной суд.Мужчина утверждает, что право на соответствующий статус есть у всех лиц, направленных на работу по обеспечению выполнения спецзадач на территории Сирии и отработавших установленный срок.Он представил в суд справку из войсковой части, что выполнял задачи в зоне вооруженного конфликта. Однако в комиссии Центрального военного округа Сотникову заявили, что Минобороны не причастно к его командировкам и потому ничем ему не обязано.Операция российских Воздушно-космических сил в Сирии началась в сентябре 2015 года. Ее целью была заявлена помощь вооруженным силам Сирийской Арабского Республики в борьбе с запрещенной в России организацией «Исламским государством».*** 


Обратная связь с отделом «Общество»: 


Если вы стали свидетелем важного события, у вас есть новость, вопросы или идея для материала, напишите на этот адрес: russia@lenta-co.ru

  
  
  
  
    :focus {
      outline: none;
    }
    .user-news {
      position: relative;
    }
    .user-news__wrap {
      display: -webkit-flex;
      display: -moz-flex;
      display: -ms-flex;
      display: -o-flex;
      display: flex;
      -webkit-flex-direction: column;
      -moz-flex-direction: column;
      -ms-flex-direction: column;
      -o-flex-direction: column;
      flex-direction: column;
      color: #FFF;
      -ms-align-items: center;
      align-items: center;
      background-color: #292929;
      border-radius: 3px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      padding: 15px 10px 20px;
      position: relative;
      z-index: 1;
    }
    .user-news::before, .user-news::after{
      display: block;
      content: '';
      position: absolute;
      border-radius: 3px;
    }
    .user-news::before {
      width: calc(100% - 36px);
      height: 14px;
      bottom: -11px;
      left: 18px;
      background-color: #F3F3F3;
    }
    .user-news::after {
      width: calc(100% - 16px);
      height: 8px;
      bottom: -5px;
      left: 8px;
      background-color: #D6D6D6;
    }
    .user-news__icon {
      width: 40px;
      height: 40px;
      margin-bottom: 5px;
    }
    .user-news__text {
      text-align: center;
    }
    .user-news__title {
      font: bold 18px/24px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      margin: 0 10px 10px;
    }
    .user-news__description {
      font: 14px/18px 'PT Serif', Georgia, Arial, serif;
      opacity: 0.5;
      margin-bottom: 15px;
    }
    .user-news__link {
      color: #CC3333;
    }
    .user-news__link:hover {
      color: #FFF !important;  
    }
    .user-news__btn {
      font: bold 12px/26px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      letter-spacing: 1.2px;
      text-transform: uppercase;
      padding: 5px 16px;
      background-color: #CC3333;
      transition: background-color .2s ease;
      border-radius: 3px;
      cursor: pointer;
    }
    .user-news__btn:hover {
      background-color: #B72E2E;
    }
    /* конец стилей плашки сниппета */
    .user-news__overlay, .user-news__form {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      z-index: 1000;
    }
    .user-news__overlay {
      background-color: #292929;
      opacity: 0.6;
      cursor: pointer;
    }
    .user-news__overlay._is-hidden {
      visibility: hidden;
    }
    .user-news__form {
      display: -webkit-flex;
      display: -moz-flex;
      display: -ms-flex;
      display: -o-flex;
      display: flex;
      -webkit-flex-direction: column;
      -moz-flex-direction: column;
      -ms-flex-direction: column;
      -o-flex-direction: column;
      flex-direction: column;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      background-color: #F3F3F3;
      top: -70px;
      transform: translateY(70px);
      padding: 5px 10px 80px;
      margin: 0;
      overflow: hidden;
      -webkit-overflow-scrolling: touch;
      font-size: 0;
    }
    .user-news__form._is-hidden {
      visibility: hidden;
    }
    .user-news__form-title {
      text-align: center;
      font: 20px/28px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      margin: 15px 0 10px;
    }
    .user-news__form-title._success {
      padding: 75px 60px;
      margin: auto;
    }
    .user-news__form-story-wrap {
      position: relative;
      margin-bottom: 30px;
    }
    .user-news__form-story {
      display: block;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      width: 100%;
      min-height: 162px;
      max-height: 70vh;
      resize: none;
      border: none;
      background-color: #FFF;
      border-radius: 3px;
      padding: 5px 5px 30px;
      font: 14px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      margin: 0;
    }
    .user-news__form-story-counter {
      position: absolute;
      right: 10px;
      bottom: 5px;
      font: 12px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      color: #292929;
      opacity: 0.2;
    }
    .user-news__form-story-counter._shifted {
      right: 20px;
    }
    .user-news__form-story-counter::before {
      content: 'Осталось символов: '
    }
    .user-news__form-story-counter._overflow {
      opacity: 1;
      color: #C33;
    }
    .user-news__form-contacts {
      margin-bottom: 20px;
    }
    .user-news__form-group {
      position: relative;
      font: 16px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      padding-bottom: 16px;
      margin-bottom: 5px;
    }
    .user-news__form-label {
      opacity: 0.2;
      position: absolute;
      top: 5px;
      left: 1px;
      cursor: text;
      transition: top .2s ease, font-size .2s ease;
    }
    .user-news__form-input {
      font: 16px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      border: none;
      border-bottom: 1px solid #D5D5D5;
      border-radius: 0;
      padding: 1px 1px 3px 1px;
      background-color: transparent;
      outline: none;
      width: 100%;
      margin: 0;
    }
    .user-news__form-input:focus + .user-news__form-label,
    .user-news__form-input._filled + .user-news__form-label {
      top: 24px;
      font-size: 12px;
    }
    .user-news__form-error {
      position: absolute;
      width: 100%;
      background-color: #F3F3F3;
      font: 16px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      color: #CC3333;
      top: 27px;
      left: 0;
      right: 0;
      font-size: 12px;
    }
    .user-news__form-error._story-error {
      top: auto;
      bottom: 0;
    }
    .user-news__form-error._submit-error {
      top: -20px;
      bottom: auto;
    }
    .user-news__form-error._is-hidden {
      visibility: hidden;
    }
    .user-news__form-submit-wrap {
      position: relative;
    }
    .user-news__form-submit {
      -moz-appearance: none;
      -webkit-appearance: none;
      font: bold 12px/26px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      letter-spacing: 1px;
      color: #FFFFFF;
      text-transform: uppercase;
      background-color: #CC3333;
      padding: 6px 10px;
      margin: 0;
      border: none;
      outline: none;
      border-radius: 3px;
      cursor: pointer;
      width: 114px;
      transition: background-color .2s ease;
    }
    .user-news__form-submit:hover {
      background-color: #B72E2E;
    }
    .user-news__form-submit._with-margin {
      margin-bottom: 30px;
    }
    .user-news__form._success {
      margin: auto;
    }
    .user-news__form-close {
      position: absolute;
      top: 5px;
      right: 5px;
      cursor: pointer;
      transition: opacity 0.2s ease;
    }
    .user-news__form-close:hover {
      opacity: 0.8;
    }
    /* desktop snippet styles: */
    @media (min-width: 600px) {
      .user-news::before, .user-news::after {
        display: none;
      }
      .user-news__wrap {
        -webkit-flex-direction: row;
        -moz-flex-direction: row;
        -ms-flex-direction: row;
        -o-flex-direction: row;
        flex-direction: row;
        justify-content: space-between;
        padding: 15px 20px 20px 15px;
      }
      .user-news__icon {
        margin: 0;
      }
      .user-news__title {
        margin: 0 0 5px 0;
        line-height: 28px;
      }
      .user-news__text {
        width: 384px;
        text-align: left;
      }
      .user-news__description {
        margin-bottom: 0;
      }
      .user-news__btn {
        -ms-align-self: flex-end;
        align-self: flex-end;
      }
      .user-news__btn_extend {
        display: none;
      }
    }
    /* desktop form styles */
    .user-news__form._desktop {
      width: 880px;
      height: 80vh;
      margin: auto;
      padding: 30px 40px;
    }
    .user-news__form._desktop._success {
      height: 220px;
    }
    .user-news__form._desktop .user-news__form-title {
      text-align: left;
      margin: 0 0 10px;
    }
    .user-news__form._desktop .user-news__form-title._success {
      padding: 70px;
      margin: auto;
      text-align: center;
    }
    .user-news__form._desktop .user-news__form-story {
      min-height: calc(80vh - 232px);
      padding: 8px 8px 30px;
      font: 14px/18px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .user-news__form._desktop .user-news__form-story-counter {
      bottom: 10px;
    }
    .user-news__form._desktop .user-news__form-contacts {
      display: -webkit-flex;
      display: -moz-flex;
      display: -ms-flex;
      display: -o-flex;
      display: flex;
    }
    .user-news__form._desktop .user-news__form-group {
      margin-right: 20px;
      width: 100%;
    }
    .user-news__form._desktop .user-news__form-group:last-child {
      margin-right: 0;
    }
    .user-news__form._desktop .user-news__form-input {
      padding: 1px 1px 5px 1px;
    }
    .user-news__form._desktop .user-news__form-input:focus + .user-news__form-label,
    .user-news__form._desktop .user-news__form-input._filled + .user-news__form-label {
      top: 24px;
      font-size: 12px;
    }
    .user-news__form._desktop .user-news__form-label {
      top: 1px;
    }
    .user-news__form._desktop .user-news__form-error._story-error {
      bottom: -20px;
    }
    .user-news__form._desktop .user-news__form-submit {
      margin: 0;
    }
    .user-news__form._desktop .user-news__form-close {
      top: 10px;
      right: 10px;
    }
  


  
    
      
        
          
            
            
            
            
            
            
            
          
        
      
      
        
          Поделитесь своей историей
        
        
          Редакция ищет врачей и медицинских работников, которые из-за своей работы сталкивались с правоохранительными органами. В последнее время все чаще говорят о том, что силовики открыли охоту на медработников. В Следственном комитете даже создано специальное подразделение, которое будет заниматься исключительно преступлениями в медицине.
        
      
      
        Прислать
        историю
      
    
  
  
  
    
      Моя история
    
    
      
      20000
      
    
    
      
        
        Меня зовут
        
      
      
        
        Мой телефон или e-mail
        
      
      
        
        Мой город
      
    
    
      
      Не удалось отправить. Попробуйте повторить позже
    
    
      
      
    
  
  
    (function() {
      var showFormBtn = document.querySelector('.js-user-news-btn');
      var overlay = document.querySelector('.js-user-news-overlay');
      var form = document.querySelector('.js-user-news-form');
      var closeFormBtn = form.querySelector('.js-user-news-close');
      var story = form.querySelector('.js-user-news-story');
      var emailOrPhone = document.getElementById('user-news-communication');
      var counter = form.querySelector('.js-user-news-counter');
      var textInputs = form.querySelectorAll('.js-user-news-input');
      var sendFormBtn = form.querySelector('.js-user-news-submit');
      var screenHeightWithBars = window.innerHeight;
      var isDesktop = window.location.hostname.indexOf('m.lenta') < 0;

      function resizeStoryBox() {
        !isDesktop && window.innerHeight !== screenHeightWithBars ? sendFormBtn.classList.add('_with-margin') : sendFormBtn.classList.remove('_with-margin');
        var formElementsHeight = isDesktop ? 232 : (332 + sendFormBtn.parentNode.offsetHeight);
        // высота элементов формы (всех кроме textarea) и их отступов для десктопа и мобилки
        story.style.height = (form.offsetHeight - formElementsHeight) + 'px';
      }

      function showForm() {
        overlay.classList.remove('_is-hidden');
        form.classList.remove('_is-hidden');
        story.focus();
        resizeStoryBox();
      }

      function closeForm() {
        overlay.classList.add('_is-hidden');
        form.classList.add('_is-hidden');
      }

      function updateStoryCounter() {
        counter.textContent = story.maxLength - story.textLength;
        story.maxLength <= story.textLength ? counter.classList.add('_overflow') : counter.classList.remove('_overflow');
        story.scrollTop = story.scrollHeight;
        story.scrollHeight > story.offsetHeight ? counter.classList.add('_shifted') : counter.classList.remove('_shifted');
      }

      function keepInputFilled(input) {
        var inputError = input.parentNode.querySelector('.js-user-news-error');
        input.addEventListener('input', function() {
          input.value.length === 0 ? input.classList.remove('_filled') : input.classList.add('_filled');
          inputError && inputError.classList.add('_is-hidden');
        });
      }

      function validateEmailOrPhone(input) {
        var emailRegexp = /[а-яА-ЯЁё\w\.\-\+:]+@[а-яА-ЯЁё\w\.\-\+:]+/;
        var inputError = input.parentNode.querySelector('.js-user-news-error');
        if (emailRegexp.test(input.value) || input.value.split('').filter(Number).join('').length >= 10) {
          return true;
        } else if (input.value === '') {
          inputError.textContent = 'Обязательное поле для ввода';
        } else {
          inputError.textContent = 'Номер/e-mail введен не правильно';
        }
        inputError.classList.remove('_is-hidden');
        return false;
      }

      function validateNotEmpty(input) {
        var inputError = input.parentNode.querySelector('.js-user-news-error');
        if (input.value.length !== 0) {
          inputError.classList.add('_is-hidden');
          return true;
        }
        inputError.textContent = 'Обязательное поле для ввода';
        inputError.classList.remove('_is-hidden');
        return false;
      }

      function validateForm() {
        var formIsValid = true;
        var fieldsToValidate = Array.prototype.slice.call(document.querySelectorAll('.js-user-news-validate'), '');
        formIsValid = fieldsToValidate.map(validateNotEmpty).reduce(function(a, b) {
          return a && b;
        });
        return formIsValid && validateEmailOrPhone(emailOrPhone);
      }

      function sendForm(e) {
        e.preventDefault();
        var formIsValid = validateForm();
        if (formIsValid) {
          var request = new XMLHttpRequest();
          var requestUrl = '/user-story';
          var requestData = {
            user_name: document.getElementById('user-news-name').value,
            user_email_or_phone: emailOrPhone.value,
            user_city: document.getElementById('user-news-city').value,
            user_story: story.value,
            url: window.location.href
          }
          var encodedRequestData = Object.keys(requestData).map(function(key) {
            return key + '=' + encodeURIComponent(requestData[key]);
          }).join('&');

          request.open('POST', requestUrl, true);
          request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
          request.onreadystatechange = function() {
            if (request.readyState === 4 && request.status === 200) {
              var successBlock = document.createElement('div');
              successBlock.classList = 'user-news__form _success';
              successBlock.innerHTML = '<div class="user-news__form-title _success">История успешно отправлена</div>';
              successBlock.appendChild(closeFormBtn);
              form.innerHTML = '';
              form.classList.add('_success');
              form.appendChild(successBlock);
            } else {
              document.getElementById('user-news-submit-error').classList.remove('_is-hidden');
            }
          };
          request.send(encodedRequestData);
          return true;
        }
        return false;
      }

      isDesktop && form.classList.add('_desktop');

      showFormBtn.addEventListener('click', showForm);
      closeFormBtn.addEventListener('click', closeForm);
      overlay.addEventListener('click', closeForm);
      story.addEventListener('input', updateStoryCounter);
      sendFormBtn.addEventListener('click', sendForm);
      Array.prototype.slice.call(textInputs, '').forEach(keepInputFilled);
    })();
  


