 14:13, 18 февраля 2019
Крупнейшая за20лет нелегальная партия черной икры попала навидео
Крупнейшая за последние 20 лет партия черной икры, изъятая сотрудниками правоохранительных органов в Иркутской области, попала на видео. Ролик в понедельник, 18 февраля, опубликовала пресс-служба МВД России.Водитель пытался провезти под видом рыбы горбуши почти две тонны деликатеса стоимостью около 100 миллионов рублей .На кадрах видно, как полицейские открывают контейнеры с расфасованной по упаковкам черной икрой и осматривают куски  замороженной рыбы осетровых видов, найденные в автомобиле-рефрижераторе. Далее изъятая икра проходит экспертизу в лаборатории, где ее взвешивают и с помощью анализов выявляют натуральное происхождение.Обратная связь с отделом «Силовые структуры»:


Если вы стали свидетелем важного события, у вас есть новость или идея для материала, напишите на этот адрес: crime@lenta-co.ru
Больше важных новостей в Telegram-канале «Лента дня». Подписывайтесь!
