 10:43, 19 февраля 2019
Устроившего пикет наКрасной площади уральского рабочего уволили постатье
Уральского рабочего Рустама Корелина уволили с завода «ФОРЕС» за нарушение трудовой дисциплины после устроенного им на Красной площади в Москве пикета против низких зарплат, передает «Дождь».«После Москвы на заводе я отработал несколько смен и уехал сдавать сессию в Екатеринбург. Пока я ее сдавал, каждого работника вызывали к директору и пытались давить, разгонять профсоюз. Мы только вдвоем с Дудыриным остались», — рассказал он телеканалу.Материалы по теме00:05 —  3 мая 2016«Борись или терпи»Почему в России так трудно создать настоящий профсоюзМужчина заявил, что собирается обжаловать решение в суде, так как оно связано с уголовным делом в отношении бухгалтера предприятия, возбужденного после его жалоб на махинации с зарплатами.Следователи, по словам Корелина, установили, что эта женщина присвоила около девяти миллионов рублей.31 января столичный суд оштрафовал уральца на 10 тысяч рублей за несогласованную акцию на Красной площади. Корелин требовал повысить зарплаты рабочим с его завода и позволить им раньше выходить на пенсию. После того как о пикете рассказали СМИ, мужчина получил предложение о работе из Екатеринбурга, но он отказался, отметив, что не может подвести своих товарищей.*** 


Обратная связь с отделом «Общество»: 


Если вы стали свидетелем важного события, у вас есть новость, вопросы или идея для материала, напишите на этот адрес: russia@lenta-co.ru
