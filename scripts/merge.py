#! python3


import os


CWD = os.getcwd()
content = []
file_names = os.listdir(CWD)
file_names.remove('merge.py')
for f_name in file_names:
    print(f_name)
    try:
        with open(f_name, encoding='utf-8') as f:
            content.append(f.read())
    except UnicodeDecodeError:
        with open(f_name, encoding='cp1251') as f:
            content.append(f.read())
with open('_merged.txt', 'w', encoding='utf-8') as f:
    f.write('\n'.join(content))
input('Press Enter to exit.')
