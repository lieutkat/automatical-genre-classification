"""
a script that cleans law documents corpus
"""

import re

with open(r'law_docs.txt', 'r', encoding='utf-8') as file:  # change the path to file
        text = file.read()

        # remove metadata-tags at the beginning
        begin_litter = '''"doc_id","sent_id","Ntokens","NContentwords","LEXV_TTR","LEXV_CTlemTR","avgDepLen",
        "avgTreeDep","maxDepLen","maxTreeDep","LEXC_CmaxWLchar","LEXC_CmaxWLsyll","LEXC_WLsyll","LEXC_maxWLchar",
        "LEXC_maxWLsyll","MORPH_actvPerV","MORPH_ppPerV","MORPH_pprsPerV","MORPH_prctIns","DSV_Conjunctions",
        "verbDeps","nounDeps","posTags","pravogovruNd","opdate","opnumber","classifierByIPS","keywordByIPS",
        "adopt_date","doc_number","doc_heading","doc_type_name","doc_author_name","year","text"'''
        text = re.sub(begin_litter, '', text, flags=re.S)

        # remove meta words & number sequences
        text = re.sub(r'"\d+",', '', text, flags=re.S)
        text = re.sub(r'"\d+\.\d+",', '', text, flags=re.S)
        text = re.sub(r'"\d\d\.\d\d\.\d\d\d\d",', '', text, flags=re.S)
        text = re.sub(r'[\d.;"]+,', '', text, flags=re.S)
        text = re.sub(r'"[А-Я+;А-Я-]+"', '', text, flags=re.S)

        # remove litter symbols
        text = re.sub(r'"+,', '', text, flags=re.S)
        text = re.sub(r'"-*"', '', text, flags=re.S)


with open(r'cleaned_low_docs.txt', 'w', encoding='utf-8') as file_2:  # path and name of a new file
        file_2.write(text)

print('done')

    

    

