"""
a script that cleans articles corpus
"""

import re

# additionally: 593, 490, 477, 476, 474, 464, 459, 438, 271, 163, 134, 109, 64, 34, 43, 44, 49

for i in range(1, 601):

        with open(r'{}.txt'.format(i),'r', encoding='utf-8') as file:  # change the path to file
                text = file.read()
                text = re.sub(
                        '(Литература|Список(использованной)? '
                        'литературы|СпиСок литературы|СПИСОК ЛИТЕРАТУРЫ|Библиография)(\.)?.*', '', text, flags=re.S)
                text = re.sub('-\n', '', text)
                text = re.sub('\n+', '', text)
                text = re.sub('\f.+\n', '\n', text)
                text = re.sub('\n+( )*[0-9]+( )*\n+', '', text)
                text = re.sub(r"[+-]?\d+", '', text)
                text = re.sub('[^’]’*] | (^’]’*)', '', text)

        with open(r'cleanedfin/{}.txt'.format(i), 'w', encoding='utf-8') as file_2:  # path and name of a new file
                file_2.write(text)
        print(i)

print('done')

    

    

