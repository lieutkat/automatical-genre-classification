"""
a script that cleans vk & live journal posts corpus
"""

import re

with open(r'vk_lj_corpus.txt', 'r', encoding='utf-8') as file:  # change the path to file
        text = file.read()

        # remove meta words & number sequences
        text = re.sub(r'\d+', '', text, flags=re.S)
        text = re.sub(r'\[[А-Я]+\]', '', text, flags=re.S)
        text = re.sub(r'P	.?,?', '', text, flags=re.S)
        text = re.sub(r'[A-Za-z-*]', '', text, flags=re.S)

        # remove litter symbols from text-id
        litter = '=__.'
        text = re.sub(litter, '', text, flags=re.S)
        text = re.sub(r'[+.]', '', text, flags=re.S)

        # lemmas weren't removed (stayed in brackets)

with open(r'cleaned_vk_lj_corpus.txt', 'w', encoding='utf-8') as file_2:  # path and name of a new file
        file_2.write(text)

print('done')







    

