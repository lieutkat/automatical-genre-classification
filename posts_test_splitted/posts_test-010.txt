	   "
		цитируется         	[цитировать]               	 	
		заявление          	[заявление]                	      	
		Митволя            	[Митволь]                  	      	
		в                  	[в]                        	        	
		сообщении          	[сообщение]                	      	
		прессслужбы       	[прессслужба]             	      	
		Минприроды         	[Минприроды]               	      	
	   

		В                  	[в]                        	        	
		то                 	[тот]                      	      	
		же                 	[же]                       	            	
		время              	[время]                    	      	
		специалисты        	[специалист]               	      	
		МЧС                	                      	      	
		утверждают         	[утверждать]               	 	
	   
		что                	[что]                      	            	
	   
		пробы              	[проба]                    	      	
	   
		отобранные         	[отобрать]                 	 	
		группой            	[группа]                   	      	
		Олега              	[Олег]                     	      	
		Митволя            	[Митволь]                  	      	
	   
		не                 	[не]                       	            	
		характеризуют      	[характеризовать]          	 	
		загрязненность     	[загрязнённость]           	      	
		вод                	[вода]                     	      	
		Пемзенской         	[#]             	           	
		протоки            	[протока]                  	      	
		и                  	[и]                        	            	
		Амура              	[Амур]                     	      	
		в_целом            	[в_целом]                  	           	
	   
		а                  	[а]                        	            	
		отражают           	[отразить]                 	 	
		лишь               	[лишь]                     	            	
		состояние          	[состояние]                	      	
		прибрежной         	[прибрежный]               	       	
		зоны               	[зона]                     	      	
	   "
		где                	[где]                      	      	
		ведется            	[вести]                    	 	
		строительство      	[строительство]            	      	
		дамбы              	[дамба]                    	      	
		с                  	[с]                        	        	
		использованием     	[использование]            	      	
		тяжелой            	[тяжёлый]                  	       	
		инженерно          	[инженерный]               	           	
	   
		строительной       	[строительный]             	       	
		техники            	[техника]                  	      	
	   

		Этот               	[этот]                     	      	
		вывод              	[вывод]                    	      	
	   
		опубликованный     	[опубликовать]             	 	
		на                 	[на]                       	        	
		сайте              	[сайт]                     	      	
		министерства       	[министерство]             	      	
	   
		сделан             	[сделать]                  	 	
		на                 	[на]                       	        	
		основании          	[основание]                	      	
		результатов        	[результат]                	      	
		анализа            	[анализ]                   	      	
		проб               	[проба]                    	      	
		воды               	[вода]                     	      	
		из                 	[из]                       	        	
		Пемзенской         	[#]             	           	
		протоки            	[протока]                  	      	
	   

		Как                	[как]                      	            	
		установили         	[установить]               	 	
		специалисты        	[специалист]               	      	
		лаборатории        	[лаборатория]              	      	
		Института          	[институт]                 	      	
		тектоники          	[тектоника]                	      	
		и                  	[и]                        	            	
		геофизики          	[геофизика]                	      	
		РАН                	                      	      	
	   
		в                  	[в]                        	        	
		пробах             	[проба]                    	      	
		из                 	[из]                       	        	
		точки              	[точка]                    	      	
		отбора             	[отбор]                    	      	
		со                 	[со]                       	        	
		слабым             	[слабый]                   	       	
		водо               	[вода]                     	           	
		обменом            	[обмен]                    	      	
	   
		где                	[где]                      	      	
		брала              	[взять]                    	 	
		пробы              	[проба]                    	      	
