"""
a script for automatic genre classification based on keyword lists
"""

import re


def get_keyword_list():  # do not change anything for this function

    # articles
    raw = open('keywords_articles.txt', 'r', encoding="utf8")
    raw_to_analyse = raw.read()

    keywords_articles = re.findall(r'[а-я]+', raw_to_analyse)
    keywords_articles_ten = keywords_articles[:10]  # only top-10 keywords with the highest frequency

    print(keywords_articles_ten)

    # lawdocs
    raw_2 = open('keywords_lawdocs.txt', 'r', encoding="utf8")
    raw_to_analyse_2 = raw_2.read()

    keywords_lawdocs = re.findall(r'[а-я]+', raw_to_analyse_2)
    keywords_lawdocs_ten = keywords_lawdocs[:10]  # only top-10 keywords with the highest frequency

    print(keywords_lawdocs_ten)

    # blogposts
    raw_3 = open('keywords_posts.txt', 'r', encoding="utf8")
    raw_to_analyse_3 = raw_3.read()

    keywords_blogposts = re.findall(r'[а-яё]+', raw_to_analyse_3)
    keywords_blogposts_ten = keywords_blogposts[:10]  # only top-10 keywords with the highest frequency

    print(keywords_blogposts_ten)

    # news
    raw_4 = open('keywords_news.txt', 'r', encoding="utf8")
    raw_to_analyse_4 = raw_4.read()

    keywords_news = re.findall(r'[а-яё]+', raw_to_analyse_4)
    keywords_news_ten = keywords_news[:10]  # only top-10 keywords with the highest frequency

    print(keywords_news_ten)

    return keywords_articles_ten, keywords_blogposts_ten, keywords_lawdocs_ten, keywords_news_ten


def count_occurrences(n):

    # ! CHANGE pattern
    text = open('posts_test_splitted/posts_test-0' + str(n) + '.txt', 'r', encoding="utf8")
    text_to_analyse = text.read()

    coefficient = 2  # for the first 5 most representative words

    # discourse: humanities
    # genre: research article
    research_article_flag = 0
    print()
    print('research article')
    for keyword in keywords_articles_ten[:5]:
        if keyword in text_to_analyse:
            print(keyword)
            research_article_flag += 1 * coefficient
    for keyword in keywords_articles_ten[5:]:
        if keyword in text_to_analyse:
            print(keyword)
            research_article_flag += 1
    print(research_article_flag)

    # discourse: law
    # genre: law document
    lawdoc_flag = 0
    print()
    print('lawdoc')
    for keyword in keywords_lawdocs_ten[:5]:
        if keyword in text_to_analyse:
            print(keyword)
            lawdoc_flag += 1 * coefficient
    for keyword in keywords_lawdocs_ten[5:]:
        if keyword in text_to_analyse:
            print(keyword)
            lawdoc_flag += 1
    print(lawdoc_flag)

    # genre: blogpost
    blogpost_flag = 0
    print()
    print('blogpost')
    for keyword in keywords_blogposts_ten[:5]:
        if keyword in text_to_analyse:
            print(keyword)
            blogpost_flag += 1 * coefficient
    for keyword in keywords_blogposts_ten[5:]:
        if keyword in text_to_analyse:
            print(keyword)
            blogpost_flag += 1
    print(blogpost_flag)

    # discourse: mass-media&internet
    # genre: news article
    news_article_flag = 0
    print()
    print('news article')
    for keyword in keywords_news_ten[:5]:
        if keyword in text_to_analyse:
            print(keyword)
            news_article_flag += 1 * coefficient
    for keyword in keywords_news_ten[5:]:
        if keyword in text_to_analyse:
            print(keyword)
            news_article_flag += 1
    print(news_article_flag)

    return research_article_flag, lawdoc_flag, blogpost_flag, news_article_flag


def identify_genre():

    dict_with_occurrences = dict()
    dict_with_occurrences['research article'] = research_article_flag
    dict_with_occurrences['law document'] = lawdoc_flag
    dict_with_occurrences['blogpost'] = blogpost_flag
    dict_with_occurrences['news article'] = news_article_flag
    print(dict_with_occurrences)

    values = dict_with_occurrences.values()
    values = list(values)
    print(values)
    max_value = max(values)
    print('Maximum number of keywords occurrences is', max_value)

    if research_article_flag + lawdoc_flag + blogpost_flag + news_article_flag:  # check if the sum i a real number (not 0)
        probability = max_value / (research_article_flag + lawdoc_flag + blogpost_flag + news_article_flag)
    else:
        probability = 0

    # new dictionary for exchanged keys and values (to print the genre itself)
    dict_with_occurrences_reversed = {value: key for key, value in dict_with_occurrences.items()}

    if probability:
        result = 'THE RESULT OF SCRIPT RUNNING:\nThe text provided refers to the following genre:' \
             '\n{}\nwith the probability {}'.format(dict_with_occurrences_reversed[max_value], probability)
    else:
        result = 'THE RESULT OF SCRIPT RUNNING:\nThe probability is 0 or incorrect. The genre cannot be identified.'

    # ! CHANGE model answer: research article, law document, news article, blogpost
    model_answer = 'blogpost'

    # comparison between the result and the model answer
    if dict_with_occurrences_reversed[max_value] == model_answer:
        estimation = 'RIGHT'
    else:
        estimation = 'WRONG'

    print()
    print(result)
    print(estimation)

    return result, estimation


def export_results():

    # ! CHANGE filename
    with open(r"results/posts_res.txt", "a") as file:
        file.write(str(i) + '\n' + result + '\n' + "external estimation of program's answer: " + estimation + '\n'*2)


# ! CHANGE the range
for i in range(0, 36):  # +1 to the name of the last doc
    print(i)
    keywords_articles_ten, keywords_blogposts_ten, keywords_lawdocs_ten, keywords_news_ten = get_keyword_list()
    research_article_flag, lawdoc_flag, blogpost_flag, news_article_flag = count_occurrences(i)
    result, estimation = identify_genre()
    export_results()

print('\nDONE')